package java9;

import java.util.Arrays;

public class Studentas {
    private String vardas;
    private String pavarde;
    private Integer klase;
    private Integer[] pazymiai;


    public Studentas(String vardas, String pavarde, Integer klase, Integer[] pazymiai) {
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.klase = klase;
        this.pazymiai = pazymiai;

    }

    public Double vidurkis() {
        Integer suma = 0;
        for (int i = 0; i < pazymiai.length; i++) {
            suma += pazymiai[i];
        }
        return Double.valueOf(suma / pazymiai.length);
    }

    public String toString() {
        return "Vardas: " + vardas + " Pavarde: " + pavarde + " Klase: " + klase + " Pazymiai " +
                Arrays.toString(pazymiai);
    }

//    public String getVardas() {
//        return vardas;
//    }
//
//    public void setVardas(String vardas) {
//        this.vardas = vardas;
//    }
//
//    public String getPavarde() {
//        return pavarde;
//    }
//
//    public void setPavarde(String pavarde) {
//        this.pavarde = pavarde;
//    }
//
//    public Integer getKlase() {
//        return klase;
//    }
//
//    public void setKlase(Integer klase) {
//        this.klase = klase;
//    }

    public Integer getKlase() {
        return klase;
    }

    public void setKlase(Integer klase) {
        this.klase = klase;
    }


}