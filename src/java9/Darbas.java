package java9;

import com.sun.jdi.event.StepEvent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Darbas {
    public static void main(String[] args) {
        Darbuotojas darbuotojas = new Darbuotojas("Jonas", "Jonaitis", 15, "kasininkas");
        System.out.println(darbuotojas.toString());
        darbuotojas.setVardas("Petras");
        darbuotojas.setAmzius(99);
        System.out.println(darbuotojas);

        Darbuotojas darbuotojas2 = new Darbuotojas("Andrius", "Andrelis", 24);
        System.out.println(darbuotojas2);

        String failoKelias = new File("").getAbsolutePath()
                + "/src/java9/Duomenys.txt";
        Darbuotojas[] darbuotojai = skaitymas(failoKelias);
        spausdinamDarbuotojus(darbuotojai);

    }

    public static void spausdinamDarbuotojus(Darbuotojas[] darbuotojai) {
        for (int i = 0; i < darbuotojai.length; i++) {
            System.out.println(darbuotojai[i]);
        }
    }


    public static Darbuotojas[] skaitymas(String failoKelias) {
        Darbuotojas[] darbuotojai = new Darbuotojas[3];
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();
            Integer indeksas = 0;
            while (eilute != null) {
                String[] eilDuomenys = eilute.split(" ");
                String vardas = eilDuomenys[0];
                String pavarde = eilDuomenys[1];
                Integer amzius = Integer.parseInt(eilDuomenys[2]);
                String pareigos = eilDuomenys[3];
                Darbuotojas objektas = new Darbuotojas(vardas, pavarde, amzius, pareigos);
                darbuotojai[indeksas] = objektas;
                indeksas++;
                eilute = skaitytuvas.readLine();
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return darbuotojai;
    }
}
