package java12;

public class Zvirblis extends Paukstis {
    public String gautiPavadinima() {
        return "Zvirblis";
    }

    public Integer gautiAmziu() {
        return 5;
    }

    public Double gautiSvori() {
        return 20d;
    }

}
