package java12;

public class Varna extends Paukstis {
    public String gautiPavadinima() {
        return "Varna";
     }

    public Integer gautiAmziu() {
        return 10;
    }

    public Double gautiSvori() {
        return 50d;
    }

}
