package java14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Studentas studentas = new Studentas("Almantas", "Steckevicius", 30, 4,
                "full stack");
        Darbuotojas darbuotojas = new Darbuotojas("Testas", "Testovicius", 40, 10,
                "testuotojas");

        Zmogus[] studentai= {studentas};
        Zmogus[] darbuotojai = {darbuotojas};

        System.out.println(Arrays.toString(studentai));
        System.out.println(Arrays.toString(darbuotojai));

        String failoKelias = new File("").getAbsolutePath()
                + "/src/java14/Duomenys.txt";
        Zmogus[] zmones = skaitymas(failoKelias);

    }

    public static void spausdinimas(Zmogus[] zmones) {
        for (int i = 0; i < zmones.length; i++) {
            System.out.println(zmones[i]);
        }
    }

    public static Zmogus[] skaitymas(String failoKelias) {
        Zmogus[] zmoniuArr = new Zmogus[0];
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();
            Integer indeksas = 0;
            while (eilute != null) {
                String[] eilDuomenys = eilute.split(" ");
                Zmogus obj;
                if(eilDuomenys[0].equals("S")) {
                    obj = new Studentas(eilDuomenys[1], eilDuomenys[2],
                            Integer.parseInt(eilDuomenys[3]), Integer.parseInt(eilDuomenys[4]),
                            eilDuomenys[5]);
                } else {
                    obj = new Darbuotojas(eilDuomenys[1], eilDuomenys[2],
                            Integer.parseInt(eilDuomenys[3]), Integer.parseInt(eilDuomenys[4]),
                            eilDuomenys[5]);
                }
                zmoniuArr = pridetiElementa(zmoniuArr, obj);
                indeksas++;
                eilute = skaitytuvas.readLine();
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
        return zmoniuArr;
    }

    public static Zmogus[] pridetiElementa(Zmogus[] masyvas, Zmogus obj) {
        masyvas = Arrays.copyOf(masyvas, masyvas.length + 1);
        masyvas[masyvas.length - 1] = obj;
        return masyvas;
    }

}
