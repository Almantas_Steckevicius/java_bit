package java22;

import java.util.*;

public class ZmogusMain {

    public static void main(String[] args) {

        Zmogus obj1 = new Zmogus("Petras", "Petraitis", "LT123155156");
        Zmogus obj2 = new Zmogus("Antanas", "Antanaitis", "LT12142422");
        Zmogus obj3 = new Zmogus("Juozas", "Juozaitis", "LT125353156");
        Zmogus obj4 = new Zmogus("Ignas", "Ignaitis", "LT124775856");
        Zmogus obj5 = new Zmogus("Kazys", "Kazaitis", "LT122757586");

        Map<String, Zmogus> mapas = new TreeMap<>();

        Zmogus[] darbuotojai = new Zmogus[] {obj1, obj2, obj3, obj4, obj5};

        mapas.put(obj1.getAsmensKodas(), obj1);
        mapas.put(obj2.getAsmensKodas(), obj2);
        mapas.put(obj3.getAsmensKodas(), obj3);
        mapas.put(obj4.getAsmensKodas(), obj4);
        mapas.put(obj5.getAsmensKodas(), obj5);

        System.out.println(mapas);


    }
}
