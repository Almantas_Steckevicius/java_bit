package java26;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.rmi.MarshalledObject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class MokinysMain {
    public static void main(String[] args) {
        String duomenys = new File("").getAbsolutePath()
                + "/src/java26/mokiniai.txt";

        List<Mokinys> mokinysList = new ArrayList<>();
        skaitymas(duomenys, mokinysList);
        System.out.println(mokinysList);

        List<Mokinys> pirmasKetvirtis = gautiMokiniusPagalKetvirti(mokinysList, 1);
        System.out.println(pirmasKetvirtis);

        List<Mokinys> antrasKetvirtis = gautiMokiniusPagalKetvirti(mokinysList, 2);
        System.out.println(antrasKetvirtis);

        List<Mokinys> treciasKetvirtis = gautiMokiniusPagalKetvirti(mokinysList, 3);
        System.out.println(treciasKetvirtis);

        List<Mokinys> ketvirtasKetvirtis = gautiMokiniusPagalKetvirti(mokinysList, 4);
        System.out.println(ketvirtasKetvirtis);

        Mokinys mokinys = rastiVyriausia(mokinysList);
        System.out.println(mokinys);

    }

    public static Mokinys rastiVyriausia(List<Mokinys> mokinysList) {
        Long max = 0L;
        Integer vyriausioIndeksas = 0;
        LocalDate dabartineData = LocalDate.now();
        ZoneId zoneId = ZoneId.systemDefault();
        long dabartine = dabartineData.atStartOfDay(zoneId).toEpochSecond();

        for (int i = 0; i < mokinysList.size(); i++) {
            long gimimo = mokinysList.get(i).getGimimoData().atStartOfDay(zoneId).toEpochSecond();
            Long amzius = dabartine - gimimo;

            if (max < amzius) {
                max = amzius;
                vyriausioIndeksas = i;
            }
        }
        return mokinysList.get(vyriausioIndeksas);
    }

    public static List<Mokinys> gautiMokiniusPagalKetvirti(List<Mokinys> mokinysList, Integer ketvirtis) {
        List<Mokinys> atrinkti = new ArrayList<>();
        for (Mokinys mokinys: mokinysList) {
            Integer mokinioMenuo = (mokinys.getGimimoData().getMonthValue());
            if (ketvirtis == 1 && mokinioMenuo < 4) {
                atrinkti.add(mokinys);
            } else if (ketvirtis == 2 && mokinioMenuo > 3 && mokinioMenuo < 7) {
                atrinkti.add(mokinys);
            } else if (ketvirtis == 3 && mokinioMenuo > 6 && mokinioMenuo < 10) {
                atrinkti.add(mokinys);
            } else if (ketvirtis == 4 && mokinioMenuo > 9) {
                atrinkti.add(mokinys);
            }
        }

        return atrinkti;
    }

    public static void skaitymas(String failas, List<Mokinys> mokinysList) {
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failas))) {
            String eilute = skaitytuvas.readLine();
            Integer kiekEiluciu = Integer.parseInt(eilute);
            eilute = skaitytuvas.readLine();
            for (int i = 0; i < kiekEiluciu; i++) {
                String[] eilDuomenys = eilute.split(" ");
                String vardas = eilDuomenys[0];
                String pavarde = eilDuomenys[1];
                LocalDate data = LocalDate.parse(eilDuomenys[2]);
                Mokinys mokinys = new Mokinys(vardas, pavarde, data);
                mokinysList.add(mokinys);
                eilute = skaitytuvas.readLine();
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
