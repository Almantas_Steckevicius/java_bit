package java13;

public class Kvadratas extends Figura {
    private Double krastine;

    public Kvadratas(String pavadinimas, Double krastine) {
        super(pavadinimas);
        this.krastine = krastine;
    }

    public String toString() {
        return "Figuros pavadinimas: " + getPavadinimas() +
                " Plotas: " + gautiPlota()
                + " Perimetras: " + gautiPerimetra() + "\n";
    }


    public Double gautiPerimetra() {
        return krastine * 4;
    }


    public Double gautiPlota() {
        return krastine * krastine;
    }

    public Double getKrastine() {
        return krastine;
    }

    public void setKrastine(Double krastine) {
        this.krastine = krastine;
    }

}
