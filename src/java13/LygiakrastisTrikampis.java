package java13;

public class LygiakrastisTrikampis extends Figura {
    private Double krastine;

    public LygiakrastisTrikampis(String pavadinimas, Double krastine) {
        super(pavadinimas);
        this.krastine = krastine;
    }

    public String toString() {
        return "Figuros pavadinimas: " + getPavadinimas() +
                " Plotas: " + gautiPlota()
                + " Perimetras: " + gautiPerimetra();
    }

    public Double gautiPerimetra() {
        return krastine * 3;
    }

    public Double gautiPlota() {
        return krastine * krastine * Math.sqrt(3) / 4;
    }

}
