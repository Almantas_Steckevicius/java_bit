package java13;

public class Apskritimas extends Figura {
    public Double spindulys;

    public Apskritimas (String pavadinimas, Double spindulys) {
        super(pavadinimas);
        this.spindulys = spindulys;
    }

    public String toString() {
        return "Figuros pavadinimas: " + getPavadinimas() + " Perimetras: " + gautiPerimetra()
                + " Plotas: " + gautiPlota() + "\n";
    }

    public Double gautiPerimetra() {
        return 2 * Math.PI * spindulys;
    }

    public Double gautiPlota() {
        return Math.PI * (spindulys * spindulys);
    }
}
