package java13;

import java.util.Arrays;

public class FiguraMain {
    public static void main(String[] args) {
        Apskritimas apskritimas = new Apskritimas("apskritimas", 3d);
        Kvadratas kvadratas = new Kvadratas("kvadratas", 5d);
        LygiakrastisTrikampis lygiakrastisTrikampis = new LygiakrastisTrikampis("Lygiakrastis trikmapis",
                7d);

        Figura[] figuros = {apskritimas, kvadratas, lygiakrastisTrikampis};

        System.out.println(Arrays.toString(figuros));
    }
}
