package java3;

import java.util.Scanner;

public class treniruote {
    public static void main(String[] args) {
        Scanner skaiciuotuvas = new Scanner(System.in);
        System.out.println("Kiek treniruotes trunka pirmadienį?");
        Double pirmadienis = skaiciuotuvas.nextDouble();
        System.out.println("Kiek treniruotes trunka antradienį?");
        Double antradienis = skaiciuotuvas.nextDouble();
        System.out.println("Kiek treniruotes trunka treciadienį?");
        Double treicadienis = skaiciuotuvas.nextDouble();
        System.out.println("Kiek treniruotes trunka ketvirtadienį?");
        Double ketvirtaienis = skaiciuotuvas.nextDouble();
        System.out.println("Kiek treniruotes trunka penktadienį?");
        Double penktadienis = skaiciuotuvas.nextDouble();
        skaiciuotuvas.close();

        Double bendrasLaikasVal = pirmadienis + antradienis + treicadienis + ketvirtaienis + penktadienis;
        Double bendrasLaikasMin = bendrasLaikasVal * 60;
        System.out.println("Per savaite treniruotciu " + bendrasLaikasMin + " minuciu");
    }
}
