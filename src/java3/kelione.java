package java3;

import java.util.Scanner;

public class kelione {
    public static void main(String[] args) {
        Scanner skaiciuotuvas = new Scanner(System.in);
        System.out.println("Kiek kuro sunaudoja 100 km?");
        Double simtasKm = skaiciuotuvas.nextDouble();
        System.out.println("Kiek km kelione?");
        Double kelionesKm = skaiciuotuvas.nextDouble();
        System.out.println("Kiek zmoniu vyksta?");
        Integer zmones = skaiciuotuvas.nextInt();
        System.out.println("Kiek kainuoja litras kuro");
        Double litroKaina = skaiciuotuvas.nextDouble();
        skaiciuotuvas.close();

        Double kiekDegaluViso = (simtasKm * kelionesKm) / 100;
        Double kelionesDegaluKaina = litroKaina * kiekDegaluViso;
        Double kelioneKainaZmogui = kelionesDegaluKaina / zmones;
        String rezult = String.format("% .2f", kelioneKainaZmogui);
        System.out.println("Keliones kaina zmogui " + rezult + " Litu");

    }
}
