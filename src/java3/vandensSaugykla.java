package java3;

import java.util.Scanner;

public class vandensSaugykla {
    public static void main(String[] args) {
        Scanner skaiciuotuvas = new Scanner(System.in);
        System.out.println("Kiek saugykloje yra kub. metru vandens?");
        Double vandensKiekis = skaiciuotuvas.nextDouble();
        System.out.println("Kiek zmoniu naudoja vandeni?");
        Double zmones = skaiciuotuvas.nextDouble();
        System.out.println("Kiek vandens sunaudoja zmogus per para?");
        Double zmogausVanduo = skaiciuotuvas.nextDouble();
        skaiciuotuvas.close();

        Double zmoniuVanduo = zmones * zmogausVanduo;
        Double paruUzteks = vandensKiekis / zmoniuVanduo;
        System.out.println("Kiek paru uzteks vandens " + paruUzteks);
    }
}
