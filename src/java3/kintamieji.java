package java3;

import java.util.Scanner;

public class kintamieji {
    public static void main(String[] args) {
        Scanner skaiciuotuvas = new Scanner(System.in);
        System.out.println("Iveskita reiksme");
        Object kintamasis = skaiciuotuvas.next();

        if (kintamasis instanceof String) {
            System.out.println("Kintamasis yra String tipo");
        }  else if (kintamasis instanceof Integer) {
            System.out.println("Kintamasis yra Integer tipo");
        }
    }
}
