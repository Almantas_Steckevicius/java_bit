package java1;

import java.util.Scanner;

public class Automobilis {
    public static void main(String[] args) {
        Scanner skaiciuotuvas = new Scanner(System.in);
        System.out.println("Koks automobilio greitis?");
        Integer greitis = skaiciuotuvas.nextInt();
        System.out.println("Koks yra tunelio ilgis?");
        Integer kelias = skaiciuotuvas.nextInt();
        skaiciuotuvas.close();

        double greitisMetrais = greitis / 3.6;
        double laikas = kelias / greitisMetrais;

        System.out.println("Automobilis tunelį pravažiuos per " + laikas + " sekundžių");

    }
}
