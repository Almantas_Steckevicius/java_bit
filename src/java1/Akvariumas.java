package java1;

import java.util.Scanner;

public class Akvariumas {
    public static void main(String[] args) {
        Scanner skaiciuotuvas = new Scanner(System.in);
        System.out.println("Kiek žuvų gyvena akvariume?");
        Integer senbuves = skaiciuotuvas.nextInt();
        System.out.println("Kiek žuvų į akvariumą įdedama kiekvieną dieną?");
        Integer naujokes = skaiciuotuvas.nextInt();
        System.out.println("Kiek dienų praėjo?");
        Integer dienos = skaiciuotuvas.nextInt();
        skaiciuotuvas.close();

        Integer bendroszuvys = senbuves + (naujokes * dienos);
        System.out.println("Kiek žuvų gyvena? " + bendroszuvys);
    }
}
