package java1;

import java.util.Scanner;

public class PamokuSkaicius {
    public static void main(String[] args) {
        Scanner skaiciuotuvas = new Scanner(System.in);
        System.out.println("Kiek pamokų yra pirmadienį?");
        Integer pirmadienis = skaiciuotuvas.nextInt();
        System.out.println("Kiek pamokų yra antradienį?");
        Integer antradienis = skaiciuotuvas.nextInt();
        System.out.println("Kiek pamokų yra trečiadienį?");
        Integer treciadienis = skaiciuotuvas.nextInt();
        System.out.println("Kiek pamokų yra ketvirtadienį?");
        Integer ketvirtadienis = skaiciuotuvas.nextInt();
        System.out.println("Kiek pamokų yra penktadienį?");
        Integer penktadienis = skaiciuotuvas.nextInt();
        skaiciuotuvas.close();

        Integer PamokuSkaicius = pirmadienis + antradienis + treciadienis + ketvirtadienis + penktadienis;
        System.out.println("Pamokų skaičius: " + PamokuSkaicius);

        Integer MinuciuSkaicius = PamokuSkaicius * 45;
        System.out.println("Tai sudaro minučių: " + MinuciuSkaicius);
    }
}
