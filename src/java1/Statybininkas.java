package java1;

import java.util.Scanner;

public class Statybininkas {
    public static void main(String[] args) {
        Scanner skaiciuotuvas = new Scanner(System.in);
        System.out.println("Sienos ilgis");
        Integer ilgis = skaiciuotuvas.nextInt();
        System.out.println("Sienos aukštis");
        Integer aukstis = skaiciuotuvas.nextInt();
        System.out.println("Plytos kaina");
        double kaina = skaiciuotuvas.nextDouble();
        skaiciuotuvas.close();


        Integer sienosPlotas = ilgis * aukstis;
        double plytosPlotas = 0.1 * 0.2;
        double plytuSkaicius = sienosPlotas / plytosPlotas;

        // apvalinimas, f kiek skaiciu po kablelio
        String rezult = String.format("% .0f", plytuSkaicius);
        System.out.println("Plytų kiekis " + rezult);

        double plytuKaina = plytuSkaicius * kaina;
        String rezults = String.format("% .2f", plytuKaina);
        System.out.println("Plytų kaina " + rezults + " Litų");

    }
}
