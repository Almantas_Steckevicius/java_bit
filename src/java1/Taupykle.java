package java1;

import java.util.Scanner;

public class Taupykle {
    public static void main(String[] args) {
        Scanner skaiciuotuvas = new Scanner(System.in);
        System.out.println("Kiek yra monetų po 5 ct?");
        Integer nikeliai = skaiciuotuvas.nextInt();
        System.out.println("Kiek yra monetų po 20 ct?");
        Integer dvidesimtines = skaiciuotuvas.nextInt();
        System.out.println("Kiek yra monetų po 2 Lt?");
        Integer dulitis = skaiciuotuvas.nextInt();
        skaiciuotuvas.close();

        double suma = (nikeliai * 0.05) + (dvidesimtines * 0.2) + (dulitis * 2);
        System.out.println("Taupyklėje yra: " + suma);

    }
}
