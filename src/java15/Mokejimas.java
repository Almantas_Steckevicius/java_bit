package java15;

interface Mokejimas {

    String gautiBankoSaskaita();
    String gautiSaskaitosAsmensVarda();
    Double gautiSuma();
}
