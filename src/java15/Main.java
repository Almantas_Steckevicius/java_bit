package java15;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Klientas klientas1 = new Klientas("LT5165165161", "Testas", 1556.0,
                new Double[]{25.0, 29.9, 500.0});
        Klientas klientas2 = new Klientas("LT5122254445", "Bandymas", 152351.0,
                new Double[]{24.0, 25.9, 1425.0});
        Klientas klientas3 = new Klientas("LT54255252825", "Testovicius", 100000.0,
                new Double[]{50.0, 99.9, 100.0, 54.0, 159.25});

        Klientas[] klientai = {klientas1, klientas2, klientas3};
        System.out.println(Arrays.toString(klientai));

        Klientas[] atrinkti = gautiKlientusSuDideliuVidurkiu(klientai);
        System.out.println(Arrays.toString(atrinkti));
    }

    public static Klientas[] gautiKlientusSuDideliuVidurkiu(Klientas[] masyvas) {
        Klientas[] atrinkti = new Klientas[0];
        Double mokejimuVidurkis = mokejimuVidurkis(masyvas);
        for (int i = 0; i < masyvas.length; i++) {
            Double[] sumuMasyvas = masyvas[i].getSumuMasyvas();
            for (int j = 0; j < sumuMasyvas.length; j++) {
                if (sumuMasyvas[j] > mokejimuVidurkis * 2) {
                    atrinkti = pridetiElementa(atrinkti, masyvas[i]);
                }
            }
        }
        return atrinkti;
    }

    public static Klientas[] pridetiElementa(Klientas[] masyvas, Klientas elementas) {
        masyvas = Arrays.copyOf(masyvas, masyvas.length + 1);
        masyvas[masyvas.length - 1] = elementas;
        return masyvas;
    }

    public static Double mokejimuVidurkis(Klientas[] masyvas) {
        Double suma = 0d;
        Integer mokejimuKiekis = 0;
        for (int i = 0; i < masyvas.length; i++) {
            Double[] sumuMasyvas = masyvas[i].getSumuMasyvas();
            for (int j = 0; j < sumuMasyvas.length; j++) {
                suma += sumuMasyvas[j];
                mokejimuKiekis++;
            }

        }
        return suma / mokejimuKiekis;
    }
}

