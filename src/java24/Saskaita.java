package java24;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Saskaita {
    private String klientoVardas;
    private LocalDate saskaitosDiena;
    private BigDecimal suma;

    public Saskaita (String klientoVardas, LocalDate saskaitosDiena, BigDecimal suma) {
        this.klientoVardas = klientoVardas;
        this.saskaitosDiena = saskaitosDiena;
        this.suma = suma;
    }

    public String toString() {
        return "Kliento vardas: " + getKlientoVardas() + " Saskaitos diena: " +
                getSaskaitosDiena() + " suma: " + getSuma();
    }

    public String getKlientoVardas() {
        return klientoVardas;
    }

    public LocalDate getSaskaitosDiena() {
        return saskaitosDiena;
    }

    public BigDecimal getSuma() {
        return suma;
    }

    public void setKlientoVardas(String klientoVardas) {
        this.klientoVardas = klientoVardas;
    }

    public void setSaskaitosDiena(LocalDate saskaitosDiena) {
        this.saskaitosDiena = saskaitosDiena;
    }

    public void setSuma(BigDecimal suma) {
        this.suma = suma;
    }
}






