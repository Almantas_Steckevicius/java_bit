package java24;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class SaskaitaMain {
    public static void main(String[] args) {
        List<Saskaita> list = new ArrayList<>();
        list.add(new Saskaita("Petras", LocalDate.parse("2019-01-01"), BigDecimal.valueOf(500.0)));
        list.add(new Saskaita("Jonas", LocalDate.parse("2019-01-05"), BigDecimal.valueOf(600.0)));

        Spausdinti(list, "bandymas");
    }


    public static void Spausdinti(List<Saskaita> list, String spausdinimoPav) {
        System.out.println(spausdinimoPav);
        for (Saskaita obj: list) {
            System.out.println(obj);
        }
        System.out.println("-----------------");
    }
}

