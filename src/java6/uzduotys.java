package java6;

import javax.security.auth.callback.CallbackHandler;
import java.util.Arrays;

public class uzduotys {
    public static void main(String[] args) {
        Integer[] masyvas = {1, 2, 5, 6, 4, 8, 9, 11, 14, 6};
        String[] zodziai = {"a", "testas", "abcd", "nebesinusikiskiakopusteliaudamiesi"};
        spausdinti(masyvas);
        spausdinti(zodziai);

        Integer suma = suma(9, 9);
        System.out.println("Suma = " + suma);

        arLyginis(3);

        Double vidurkis = vidurkis(masyvas);
        System.out.println("Vidurkis =  " + vidurkis);

        Integer max = didziausiasSkaicius(masyvas);
        System.out.println("Didziausias skaicius = " + max);

        Integer min = maziausiasSkaicius(masyvas);
        System.out.println("Maziausias skaicius = " + min);

        didejimoTvarka(masyvas);
        spausdinti(masyvas);

        Integer[] naujasMasyvas = visiSkaiciaiMazesniUz10(masyvas);
        spausdinti(naujasMasyvas);

        String ilgiausiasZodis = ilgiausiasZodis(zodziai);
        System.out.println(ilgiausiasZodis);

        Character vidurineRaide = vidurinisSimbolis(ilgiausiasZodis);
        System.out.println(vidurineRaide);

    }

    public static void didejimoTvarka(Integer[] masyvas) {
        int temp;
        for (int i = 1; i < masyvas.length; i++) {
            for (int j = i; j > 0; j--) {
                if (masyvas[j] < masyvas [j - 1]) {
                    temp = masyvas[j];
                    masyvas[j] = masyvas[j - 1];
                    masyvas[j - 1] = temp;
                }
            }
        }
     }


    public static Integer didziausiasSkaicius(Integer[] masyvas) {
        Integer max = 0;
        for (int i = 1; i < masyvas.length; i++) {
            if (masyvas[i] > max) {
                max = masyvas[i];
            }
        }
        return max;
    }

    public static Integer maziausiasSkaicius(Integer[] masyvas) {
        Integer min = 100;
        for (int i = 0; i < masyvas.length; i++) {
            if (masyvas[i] < min) {
                min = masyvas[i];
            }
        }
        return min;
    }


    public static Double vidurkis(Integer[] masyvas) {
        Double suma = 0d;
        for (int i = 0; i < masyvas.length; i++) {
            suma += masyvas[i];
        }
        return suma / masyvas.length;
    }

    public static void arLyginis(Integer skaicius) {
        if(skaicius % 2 == 0) {
            System.out.println(skaicius + " Skaicius yra lyginis");
            } else {
            System.out.println(skaicius + " Skaicius yra nelyginis");
            }
    }

    public static Integer suma(Integer a, Integer b) {
        return a + b;
    }

    public static void spausdinti(Integer[] masyvas) {
        for (int i = 0; i < masyvas.length; i++) {
            //arLyginis(masyvas[i]);
            System.out.println(masyvas[i]);
        }
    }

    public static void spausdinti(String[] masyvas) {
        for (int i =0; i < masyvas.length; i++) {
            System.out.println(masyvas[i]);
        }
    }

    public static Integer[] visiSkaiciaiMazesniUz10(Integer[] skaiciai) {
        Integer[] naujasMasyvas = new Integer[0];
        for (int i = 0; i < skaiciai.length; i++) {
            if (skaiciai[i] < 10) {
                naujasMasyvas = pridetiElementa(naujasMasyvas, skaiciai[i]);
            }
        }
        return naujasMasyvas;
    }

    public static Integer[] pridetiElementa(Integer[] masyvas, Integer elementas) {
        masyvas = Arrays.copyOf(masyvas, masyvas.length + 1);
        masyvas[masyvas.length - 1] = elementas;
        return masyvas;
    }

    public static String ilgiausiasZodis(String[] zodziai) {
        String ilgiausiasZodis = "";
        for (int i = 0; i < zodziai.length; i++) {
            if (zodziai[i].length() > ilgiausiasZodis.length()) {
                ilgiausiasZodis = zodziai[i];
            }
        }
        return ilgiausiasZodis;
    }

    public static Character vidurinisSimbolis(String zodis) {
        Integer vidurinesRaidesIndeksas = zodis.length() / 2;
        return zodis.charAt(vidurinesRaidesIndeksas);
    }
}
