package java17;

import java.util.Arrays;

public class DarbuotojasMain {
    public static void main(String[] args) {
        Darbuotojas darbuotojas1 = new Darbuotojas("Petras", 500d,
                new Darbuotojas.Adresas("Kaunas", "Savanoriu", 15));

        Darbuotojas darbuotojas2 = new Darbuotojas("Antanas", 600d,
                new Darbuotojas.Adresas("Kaunas", "Upes", 25));

        Darbuotojas darbuotojas3 = new Darbuotojas("Vytas", 700d,
                new Darbuotojas.Adresas("Klaipeda", "Herkaus", 14));

        Darbuotojas darbuotojas4 = new Darbuotojas("Kazys", 800d,
                new Darbuotojas.Adresas("Klaipeda", "Saules", 6));

        Darbuotojas darbuotojas5 = new Darbuotojas("Juozas", 900d,
                new Darbuotojas.Adresas("Panevezys", "Didzioji", 151));

        Darbuotojas[] darbuotojai = new Darbuotojas[] {darbuotojas1, darbuotojas2, darbuotojas3, darbuotojas4,
        darbuotojas5};

        String[] miestai = skirtingiMiestai(darbuotojai);
        System.out.println(Arrays.toString(miestai));

    }


    public static String[] skirtingiMiestai(Darbuotojas[] darbuotojai) {
        String[] unikalusMiestai = new String[0];

        for (int i = 0; i < darbuotojai.length; i++) {
            String miestas = darbuotojai[i].getAdresas().getMiestas();
            if (i == 0) {
                unikalusMiestai = pridetiElementa(unikalusMiestai, miestas);
            }
            for (int j = 0; j < unikalusMiestai.length; j++) {
                if (!unikalusMiestai[j].equals(miestas)) {
                    unikalusMiestai = pridetiElementa(unikalusMiestai, miestas);
                }
            }
        }

        return unikalusMiestai;
    }


    public static String[] pridetiElementa(String[] masyvas, String elementas) {
        masyvas = Arrays.copyOf(masyvas, masyvas.length + 1);
        masyvas[masyvas.length - 1] = elementas;
        return masyvas;
    }

}
