package java17;

public class Darbuotojas {
    private String vardas;
    private Double atlyginimas;
    private Adresas adresas;

    public Darbuotojas(String vardas, Double atlyginimas, Adresas adresas) {
        this.vardas = vardas;
        this.atlyginimas = atlyginimas;
        this.adresas = adresas;
    }

    public String toString() {
        return "Vardas = " + getVardas() + " Atlyginimas = " + getAtlyginimas() + " Adresas = " + getAdresas();
    }

    public String getVardas() {
        return vardas;
    }

    public Double getAtlyginimas() {
        return atlyginimas;
    }

    public Adresas getAdresas() {
        return adresas;
    }

    public void setVardas(String vardas) {
        this.vardas = vardas;
    }

    public void setAtlyginimas(Double atlyginimas) {
        this.atlyginimas = atlyginimas;
    }

    public void setAdresas(Adresas adresas) {
        this.adresas = adresas;
    }

    public static class Adresas {
        private String miestas;
        private String gatve;
        private Integer numeris;

        public Adresas(String miestas, String gatve, Integer numeris) {
            this.miestas = miestas;
            this.gatve = gatve;
            this.numeris = numeris;
        }

        public String toString() {
            return "Miestas = " + getMiestas() + " Gatve = " + getGatve() + " Nr. = " + getNumeris();
        }

        public String getMiestas() {
            return miestas;
        }

        public String getGatve() {
            return gatve;
        }

        public Integer getNumeris() {
            return numeris;
        }
    }
}
