package java21;

import java9.Darbuotojas;

import javax.swing.*;
import javax.xml.crypto.KeySelectorException;
import javax.xml.crypto.dsig.spec.HMACParameterSpec;
import java.io.*;
import java.util.*;

public class MasinosMain {
    public static void main(String[] args) {
        String duomenuFailoPath = new File("").getAbsolutePath()
                + "/src/java21/Duomenys.txt";
        List<Masina> masinos = new ArrayList<>();

        skaitymas(duomenuFailoPath, masinos);
//        System.out.println(masinos);

        Collections.sort(masinos);
        System.out.println(masinos);

        System.out.println("Seniausia masina: ");
        List<Masina> seniausia = seniausiaMasina(masinos);
        System.out.println(seniausia);

        System.out.println("Naujausia masina: ");
        List<Masina> naujausia= naujausiaMasina(masinos);
        System.out.println(naujausia);

        System.out.println("Masinos nuo 2000: ");
        List<Masina> atrinktos2 = rastiMasinasNuo2000(masinos, 2000);
        System.out.println(atrinktos2);

        System.out.println("Masinos tarp 2000 ir 2010 metu");
        List<Masina> atrinktos = rastiMasinasTarpMetu(masinos, 2000, 2010);
        System.out.println(atrinktos);

        System.out.println("Visi volkswagenai: ");
        List<Masina> volkswagenai = volkswagenai(masinos);
        System.out.println(volkswagenai);

        System.out.println("Didziausias turis: ");
        List<Masina> turis = didziausiasTuris(masinos);
        System.out.println(turis);

        System.out.println("Benzininiai automobiliai: ");
        List<Masina> benzinas = benzinas(masinos);
        System.out.println(benzinas);
    }

    public static List<Masina> seniausiaMasina(List<Masina> masinos) {
        List<Masina> seniausia = new ArrayList<>();
        Integer minMetai = 2050;
        Integer objektas = 0;

        for (int i = 0; i < masinos.size(); i++) {
            if (masinos.get(i).getMetai() < minMetai) {
                minMetai = masinos.get(i).getMetai();
                objektas = i;

            }
        }
        seniausia.add(masinos.get(objektas));
        return seniausia;
    }

    public static List<Masina> naujausiaMasina(List<Masina> masinos) {
        List<Masina> naujausia = new ArrayList<>();
        Integer minMetai = 0;
        Integer objektas = 0;

        for (int i = 0; i < masinos.size(); i++) {
            if (masinos.get(i).getMetai() > minMetai) {
                minMetai = masinos.get(i).getMetai();
                objektas = i;

            }
        }
        naujausia.add(masinos.get(objektas));
        return naujausia;
    }
    public static List<Masina> rastiMasinasNuo2000(List<Masina> masinos, Integer nuo) {
        List<Masina> atrinktos2 = new ArrayList<>();
        for(int i = 0; i < masinos.size(); i++) {
            if (masinos.get(i).getMetai() > nuo) {
                atrinktos2.add(masinos.get(i));
            }
        }
        return atrinktos2;
    }

    public static List<Masina> rastiMasinasTarpMetu(List<Masina> masinos, Integer nuo, Integer iki) {
        List<Masina> atrinktos = new ArrayList<>();
        for(int i = 0; i < masinos.size(); i++) {
            if (masinos.get(i).getMetai() > nuo && masinos.get(i).getMetai() < iki) {
                atrinktos.add(masinos.get(i));
            }
        }
        return atrinktos;
    }

    public static List<Masina> volkswagenai (List<Masina> masinos) {
        List<Masina> volkswagenai = new ArrayList<>();
        for (int i = 0; i < masinos.size(); i++) {
            if (masinos.get(i).getGamintojas().equals("VW")) {
                volkswagenai.add(masinos.get(i));
            }
        }
        return volkswagenai;
    }

    public static List<Masina> didziausiasTuris(List<Masina> masinos) {
        List<Masina> turis = new ArrayList<>();
        Double minTuris = (double) 0;
        Integer objektas = 0;

        for (int i = 0; i < masinos.size(); i++) {
            if (masinos.get(i).getVariklioTuris() > minTuris) {
                minTuris = masinos.get(i).getVariklioTuris();
                objektas = i;

            }
        }
        turis.add(masinos.get(objektas));
        return turis;
    }

    public static List<Masina> benzinas (List<Masina> masinos) {
        List<Masina> benzinas = new ArrayList<>();
        for (int i = 0; i < masinos.size(); i++)
            if (masinos.get(i).getKuroTipas().equals("B")) {
                benzinas.add(masinos.get(i));
            }
        return benzinas;
    }

    public static void skaitymas (String duomenuFailoPath, List<Masina> masinos) {

        try (BufferedReader br = new BufferedReader(new FileReader(duomenuFailoPath))) {
            String eilute = br.readLine();
            Integer kiekEiluciu = Integer.parseInt(eilute);
            eilute = br.readLine();
            for (int i = 0; i < kiekEiluciu; i++) {
                String[] eilDuomenys = eilute.split(" ");

                String gamintojas = eilDuomenys[0];
                String modelis = eilDuomenys[1];
                Integer metai = Integer.parseInt(eilDuomenys[2]);
                Integer kaina = Integer.parseInt(eilDuomenys[3]);
                Double turis = Double.parseDouble(eilDuomenys[4]);
                String kuras = eilDuomenys[5];

                Masina masina = new Masina (gamintojas, modelis, metai, kaina, turis, kuras);
                masinos.add(masina);

                eilute = br.readLine();
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println("something else went wrong");
        }
    }

}
