package java11;

public class AnimalMain {
    public static void main(String[] args) {
        Cat objektas1 = new Cat("katinas ", "miau");
        System.out.println(objektas1);

        Dog objektas2 = new Dog("šuo ", "au");
        System.out.println(objektas2);

        Cow objektas3 = new Cow("karve ", "muu");
        System.out.println(objektas3);

    }
}
