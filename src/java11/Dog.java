package java11;

public class Dog extends Animal {
    private String au;

    public Dog(String vardas, String au) {
        super(vardas);
        this.au = au;
    }

    public String toString() {
        return "Gyvuno pavadinimas: " + getVardas() + "Gyvuno garsas: " + au;
    }
}
