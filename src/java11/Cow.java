package java11;

public class Cow extends Animal {
    private String muu;

    public Cow(String vardas, String muu) {
        super(vardas);
        this.muu = muu;
    }

    public String toString() {
        return "Gyvuno pavadinimas: " + getVardas() + "Gyvuno garsas: " + muu;
    }

}
