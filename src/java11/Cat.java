package java11;

public class Cat extends Animal {
    private String miau;

    public Cat(String vardas, String miau) {
        super(vardas);
        this.miau = miau;
    }

    public String toString() {
        return "Gyvuno pavadinimas: " + getVardas() + "Gyvuno garsas: " + gautiGarsa();
    }

    public String gautiGarsa() {
        return miau;
    }
}
