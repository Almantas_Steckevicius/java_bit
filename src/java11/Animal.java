package java11;

public class Animal {
    private String vardas;

    public Animal (String vardas)
    {
        this.vardas = vardas;
    }

    public String getVardas() {
        return vardas;
    }
}
