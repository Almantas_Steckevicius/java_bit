package java19;

public class BendrineKlase<T> {
    private T objektas;

    public BendrineKlase (T objektas) {
        this.objektas = objektas;
    }

    public T getObjektas() {
        return objektas;
    }

    public void setObjektas(T objektas) {
        this.objektas = objektas;
    }
}
