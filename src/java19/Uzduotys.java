package java19;

public class Uzduotys {
    public static void main(String[] args) {
        Double[] doublai = {1.0, 0.3, 3.0, 4.0};
        Integer[] skaiciai = {1, 2, 3, 4};
        Character[] charai = {'a', 'b', 'c'};
//        spausdinti(doublai);
//        spausdinti(skaiciai);
//        spausdinti(charai);

        BendrineKlase<String> obj = new BendrineKlase("testas");
        System.out.println(obj.getObjektas());

        KeliBendriniaiTipai<String, Integer> atrakinkti = new KeliBendriniaiTipai<>("spyna", 123);
        System.out.println(atrakinkti.gautiRakta());


//        public static <T > void spausdinti (T[]masyvas){
//            for (T reiksme : masyvas) {
//                System.out.print(reiksme + ", ");
//            }
//            System.out.println();
//        }
    }
}
