package java19;

public class KeliBendriniaiTipai<K,V> implements Pora {
    private K raktas;
    private V reiksme;

    public KeliBendriniaiTipai(K raktas, V reiksme) {
        this.raktas = raktas;
        this.reiksme = reiksme;
    }

    public K gautiRakta() {
        return raktas;
    }

    public V gautiReiksme() {
        return reiksme;
    }

    public void setRaktas(K raktas) {
        this.raktas = raktas;
    }

    public void setReiksme(V reiksme) {
        this.reiksme = reiksme;
    }
}
