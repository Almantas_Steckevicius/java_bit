package java2;

import java.util.Scanner;
import java.util.function.DoubleBinaryOperator;

public class didskaicius {
    public static void main(String[] args) {
        Scanner skaiciuotuvas = new Scanner(System.in);
        System.out.println("Iveskite pirma skaiciu");
        Double pirmas = skaiciuotuvas.nextDouble();
        System.out.println("Iveskite antra skaiciu");
        Double antras = skaiciuotuvas.nextDouble();
        System.out.println("Iveskite trecia skaiciu");
        Double trecias = skaiciuotuvas.nextDouble();
        skaiciuotuvas.close();

        if (pirmas > antras && pirmas > trecias) {
            System.out.println("Pirmas skaicius didziausias");
        } else if (antras > pirmas && antras > trecias) {
            System.out.println("Antras skaicius didziausias");
        } else {
            System.out.println("Trecias skaicius didziausias");
        }

    }
}
