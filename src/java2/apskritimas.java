package java2;

import java.util.Scanner;

public class apskritimas {
    public static void main(String[] args) {
        Scanner skaiciuotuvas = new Scanner(System.in);
        System.out.println("Įveskite apskritimo spinduli");
        Double a = skaiciuotuvas.nextDouble();
        skaiciuotuvas.close();

        Double apskritimas = Math.pow(a, 2) * Math.PI;
        String rezult = String.format("% .2f", apskritimas);
        System.out.println("Apskritimo plotas " + rezult);
    }
}
