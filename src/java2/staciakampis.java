package java2;

import java.util.Scanner;

public class staciakampis {
    public static void main(String[] args) {
        Scanner skaiciuotuvas = new Scanner(System.in);
        System.out.println("Įveskite staciakampio kraštinę A");
        Double a = skaiciuotuvas.nextDouble();
        System.out.println("Įveskite staciakampio kraštinę B");
        Double b = skaiciuotuvas.nextDouble();
        skaiciuotuvas.close();

        Double staciakampis = a * b;
        System.out.println("Staciakampio plotas " + staciakampis);
    }
}
