package java2;

import java.util.Scanner;

public class kvadratinelygtis {
    public static void main(String[] args) {
        Scanner skaiciuotuvas = new Scanner(System.in);
        System.out.println("Įveskite skaiciu a");
        Double a = skaiciuotuvas.nextDouble();
        System.out.println("Įveskite skaiciu b");
        Double b = skaiciuotuvas.nextDouble();
        System.out.println("Įveskite skaiciu c");
        Double c = skaiciuotuvas.nextDouble();
        skaiciuotuvas.close();

        Double d = Math.pow(b, 2) - 4 * a * c;
        System.out.println("Diskriminantas = " + d);
        if (d > 0) {
            Double x1 = (-b + Math.sqrt(d) / 2 * a);
            Double x2 = (-b - Math.sqrt(d) / 2 * a);
            System.out.println("x1 = " + x1);
            System.out.println("x2 = " + x2);
        } else if (d == 0 ){
            Double x = -b / 2 *a;
            System.out.println("x = " + x);
        }
    }
}
