package java2;

import java.util.Scanner;

class plotai {
    public static void main(String[] args) {
        Scanner skaiciuotuvas = new Scanner(System.in);
        System.out.println("Įveskite trikampio kraštinę A");
        Double a = skaiciuotuvas.nextDouble();
        System.out.println("Įveskite trikampio kraštinę B");
        Double b = skaiciuotuvas.nextDouble();
        skaiciuotuvas.close();

        Double statusis = (a * b) / 2;
        System.out.println("Staciojo trikampio plotas " + statusis);
    }
}
