package java2;

import java.util.Scanner;

public class raides {
    public static void main(String[] args) {
        Scanner skaiciuotuvas = new Scanner(System.in);
        System.out.println("Iveskita zodi");
        String zodis = skaiciuotuvas.next();
        System.out.println("Pirma zodzio raide = " + zodis.charAt(0));
        System.out.println("Paskutine zodzio raide = " + zodis.charAt(zodis.length()-1));
    }
}
