package java2;

import java.util.Scanner;

public class teigiamasneigiamas {
    public static void main(String[] args) {
        Scanner skaiciuotuvas = new Scanner(System.in);
        System.out.println("Įveskite skaiciu");
        Double a = skaiciuotuvas.nextDouble();
        skaiciuotuvas.close();

        if(a > 0 ) {
            System.out.println("Skaicius yra teigiamas");
        } else if (a < 0 ) {
            System.out.println("Skaicius yra neigiamas");
        } else {
            System.out.println("Skaicius lygus nuliui");
        }
    }
}
