package java7;

import org.w3c.dom.ls.LSOutput;

import javax.crypto.spec.PSource;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;

public class klaidos {
    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/java7/duomenys.txt";
        skaitymas("", failoKelias);


        Integer[] masyvas = {1, 2, 3, 4, 5, 666};
//        Integer reiksme = grazintiReiksme(masyvas, 99);
//        System.out.println(reiksme);

//        Integer paverstas = pakeistiTipa("5a5");
//        System.out.println(paverstas);

//        String zodis = "bandymas";
//        Character raide = gautiRaide(zodis, 3);
//        System.out.println(raide);

    }

//    public static Integer grazintiReiksme(Integer[] masyvas, Integer indeksas) {
//        try {
//            return masyvas[indeksas];
//        } catch (ArrayIndexOutOfBoundsException ex) {
//            System.out.println("Ivestas neegzistuojantis indeksas");
//            return masyvas[masyvas.length - 1];
//        }
//    }


//    public static Integer pakeistiTipa(String skaicius) {
//        try {
//            return Integer.parseInt(skaicius);
//        } catch (NumberFormatException ex) {
//            System.out.println("Nepavyko paversti i skaiciu");
//            return null;
//        }
//    }

    public static void skaitymas(String failoKelias, String visadaEgzistuojantisFailas) {
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();
            while (eilute != null) {
                System.out.println(eilute);
                eilute = skaitytuvas.readLine();
            }
        } catch (FileNotFoundException ex) {
//            System.out.println("Failas nerastas");
//            System.out.println("Naudojamas kitas egzistuojantis failas");
//            skaitymas(visadaEgzistuojantisFailas, visadaEgzistuojantisFailas);
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

//    public static Character gautiRaide(String zodis, Integer indeksas) {
//        try {
//            return zodis.charAt(indeksas);
//        } catch (Exception ex) {
//            System.out.println(ex);
//            return null;
//        }
//    }

    public static Integer[] papildomasElementas(Integer[] masyvas, int e) {

        try {
            masyvas = Arrays.copyOf(masyvas, masyvas.length + 1);
            masyvas[masyvas.length - 1] = e;
            return masyvas;
        } catch (Exception ex) {
            System.out.println(ex);
            return null;
        }
    }

    }

