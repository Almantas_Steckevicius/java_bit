package java25;

public class Butas {
    private Integer butoNr;
    private String butoAdresas;
    private Integer kambariuSk;
    private Double kvadratura;
    private Double kaina;

    public Butas(Integer butoNr, String butoAdresas, Integer kambariuSk, Double kvadratura, Double kaina) {
        this.butoNr = butoNr;
        this.butoAdresas = butoAdresas;
        this.kambariuSk = kambariuSk;
        this.kvadratura = kvadratura;
        this.kaina = kaina;
    }

    @Override
    public String toString() {
        return "Butas{" +
                "butoNr=" + butoNr +
                ", butoAdresas='" + butoAdresas + '\'' +
                ", kambariuSk=" + kambariuSk +
                ", kvadratura=" + kvadratura +
                ", kaina=" + kaina +
                '}' + "\n";
    }

    public Integer getButoNr() {
        return butoNr;
    }

    public String getButoAdresas() {
        return butoAdresas;
    }

    public Integer getKambariuSk() {
        return kambariuSk;
    }

    public Double getKvadratura() {
        return kvadratura;
    }

    public Double getKaina() {
        return kaina;
    }

    public void setButoNr(Integer butoNr) {
        this.butoNr = butoNr;
    }

    public void setButoAdresas(String butoAdresas) {
        this.butoAdresas = butoAdresas;
    }

    public void setKambariuSk(Integer kambariuSk) {
        this.kambariuSk = kambariuSk;
    }

    public void setKvadratura(Double kvadratura) {
        this.kvadratura = kvadratura;
    }

    public void setKaina(Double kaina) {
        this.kaina = kaina;
    }
}
