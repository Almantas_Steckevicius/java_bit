package java10;

public class StatusisTrikampis extends Figura {
    private Double plotis;
    private Double ilgis;
    private Double izambine;

    public StatusisTrikampis (String vardas, Double plotis, Double ilgis, Double izambine) {
        super(vardas);
        this.ilgis = ilgis;
        this.plotis = plotis;
        this.izambine = izambine;
    }

    public String toString() {
        return "Figuros pavadinimas: " + getVardas() + "\n" + " Plotas: " + gautiPlota() + "\n" + " Perimetras: "
                + gautiPerimetra();
    }

    public Double gautiPerimetra() {
        return plotis + ilgis + izambine;
    }

    public Double gautiPlota() {
        return (plotis * ilgis) / 2;
    }
}


