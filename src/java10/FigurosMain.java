package java10;

import javax.xml.crypto.dom.DOMCryptoContext;
import java.awt.event.KeyAdapter;
import java.io.*;
import java.util.Arrays;

public class FigurosMain {
    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/java10/Duomenys.txt";
        Figura[] figuros = skaityti(failoKelias);
        System.out.println(Arrays.toString(figuros));

        Figura didziausiasPlotas = didziausiasPlotas(figuros);
        System.out.println(didziausiasPlotas);

    }

    public static Figura didziausiasPlotas(Figura[] figuros) {
        Double max = 0d;
        Integer indeksas = 0;
        for (int i = 1; i < figuros.length; i++) {
            Double plotas = 0d;
            if (figuros[i] instanceof Kvadratas) {
                Kvadratas obj = (Kvadratas)figuros[i];
                plotas = obj.gautiPlota();
            } else if (figuros[i] instanceof Staciakampis) {
                Staciakampis obj = (Staciakampis)figuros[i];
                plotas = obj.gautiPlota();
            } else {
                StatusisTrikampis obj = (StatusisTrikampis) figuros[i];
                plotas = obj.gautiPlota();
            }
            if (max < plotas) {
                max = plotas;
                indeksas = i;
            }
        }
        return figuros[indeksas];
    }

    public static Figura[] skaityti(String failoKelias) {
        Figura[] figuros = new Figura[3];
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();
            Integer indeksas = 0;
            while (eilute != null) {
                String[] eilDuomenys = eilute.split(" ");
                if(eilDuomenys.length == 1) {
                    Double krastine = Double.parseDouble(eilDuomenys[0]);
                    figuros[indeksas] = new Kvadratas("kvadratas", krastine);
                } else if (eilDuomenys.length == 2) {
                    Double krastine1 = Double.parseDouble(eilDuomenys[0]);
                    Double krastine2 = Double.parseDouble(eilDuomenys[1]);
                    figuros[indeksas] = new Staciakampis("staciakampis", krastine1, krastine2);
                } else {
                    Double trikampioKrastine1 = Double.parseDouble(eilDuomenys[0]);
                    Double trikampioKrastine2 = Double.parseDouble(eilDuomenys[1]);
                    Double trikampioKrastine3 = Double.parseDouble(eilDuomenys[2]);
                    figuros[indeksas] = new StatusisTrikampis("statusis trikampis", trikampioKrastine1,
                            trikampioKrastine2, trikampioKrastine3);
                }
                indeksas++;
                eilute = skaitytuvas.readLine();
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
        return figuros;
    }
}
