package java10;

public class Kvadratas extends Figura {
    private Double krastinesIlgis;

    public Kvadratas (String vardas, Double krastinesIlgis) {
        super(vardas);
        this.krastinesIlgis = krastinesIlgis;
    }

    public String toString() {
        return "Figuros pavadinimas: " + getVardas() + "\n" + " Plotas: " + gautiPlota() + "\n" + " Perimetras: "
                + gautiPerimetra();
    }

    public Double gautiPerimetra() {
        return krastinesIlgis * 4;
    }

    public Double gautiPlota() {
        return krastinesIlgis * krastinesIlgis;
    }
}

