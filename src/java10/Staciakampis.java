package java10;

import javax.xml.crypto.dom.DOMCryptoContext;

public class Staciakampis extends Figura {
    private Double plotis;
    private Double ilgis;

    public Staciakampis (String vardas, Double plotis, Double ilgis) {
        super(vardas);
        this.ilgis = ilgis;
        this.plotis = plotis;
    }


    public String toString() {
        return "Figuros pavadinimas: " + getVardas() + "\n" + " Plotas: " + gautiPlota() + "\n" + " Perimetras: "
                + gautiPerimetra();
    }

    public Double gautiPerimetra() {
        return ilgis * 2 + plotis * 2;
    }

    public Double gautiPlota() {
        return ilgis * plotis;
    }

}
