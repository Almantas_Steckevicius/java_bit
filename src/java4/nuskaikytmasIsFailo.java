package java4;

import javafx.animation.ScaleTransition;

import java.io.*;

public class nuskaikytmasIsFailo {
    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/java4/Duomenys.txt";

        String rezultatai = new File("").getAbsolutePath()
                + "/src/java4/Rezultatai.txt";

        String duomenuTekstas = "";
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();

            while (eilute != null) {
                duomenuTekstas += "\n" + eilute;
                //System.out.println(eilute);
                String[] eilutesReiksmes = eilute.split(" ");
                Integer skaicius = Integer.parseInt(eilutesReiksmes[1]);
                System.out.println(skaicius);
                eilute = skaitytuvas.readLine();
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }

        try (BufferedWriter spausdinimas = new BufferedWriter(new FileWriter(rezultatai))) {
            spausdinimas.write("parasem i faila");
            spausdinimas.write(duomenuTekstas);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
