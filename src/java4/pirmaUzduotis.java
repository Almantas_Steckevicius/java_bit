package java4;

public class pirmaUzduotis {
    public static void main(String[] args) {
        for (int i = 0; i <= 100; i++) {
            System.out.println(i);
        }
        for (int i = 100; i >= 0; i--) {
            System.out.println(i);
        }
        // tas pats ciklas tik su while

        int i = 0;
        while (i < 100) {
            System.out.println(i);
            i++;
        }

        i = 100;
        while (i >= 0) {
            System.out.println(i);
            i--;
        }
    }
}
