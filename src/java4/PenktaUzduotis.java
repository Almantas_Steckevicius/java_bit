package java4;

public class PenktaUzduotis {
    public static void main(String[] args) {
        Integer[] masyvas = {1, 2, 3, 4, 5, 6, 7, 8, 10};

        for (Integer suma: masyvas) {

            if (suma % 2 != 0) {
                continue;
            }
            System.out.println(suma);
        }
    }
}
