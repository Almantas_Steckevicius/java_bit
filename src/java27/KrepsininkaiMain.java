package java27;

import javax.print.DocFlavor;
import java.io.*;
import java.nio.charset.IllegalCharsetNameException;
import java.util.Map;
import java.util.TreeMap;

public class KrepsininkaiMain {
    public static void main(String[] args) {
        String zaidejai = new File("").getAbsolutePath()
                + "/src/java27/Zaidejai.txt";
        String taskai = new File("").getAbsolutePath()
                + "/src/java27/Taskai.txt";
        String rezultatai = new File("").getAbsolutePath()
                + "/src/java27/suvestine.txt";

        Map<Integer, Zaidejas> krepsininkai = new TreeMap<>();

        skaitytiZaidejus(zaidejai, krepsininkai);
        skaitytiTaskus(taskai, krepsininkai);
        System.out.println(krepsininkai);
        rasymas(rezultatai, krepsininkai);
    }

    public static void rasymas(String rezultatai, Map<Integer, Zaidejas> krepsininkai) {
        try (BufferedWriter spausdinimas = new BufferedWriter(new FileWriter(rezultatai))) {
            spausdinimas.write("*********************************************************************************\n");
            spausdinimas.write("|Nr.| Vardas Pavarde          | Baudos| Dvit. | Trit. | Taskai |Nepat.|Tikslumas|\n");
            spausdinimas.write("*********************************************************************************\n");
            for (Zaidejas zaidejas : krepsininkai.values()) {
                spausdinimas.write(zaidejas.toString());
            }
            spausdinimas.write("*********************************************************************************\n");
            Zaidejas rezultatyviausias = rezultatyviausias(krepsininkai);
            spausdinimas.write("Daugiausia tasku pelne: "
                    + rezultatyviausias.getNumeris() + " "
                    + rezultatyviausias.getVardas() + " "
                    + rezultatyviausias.getPavarde() + " "
                    + rezultatyviausias.getTaskai() + "\n");

            Zaidejas tritaskininkas = daugiausiaTritaskiu(krepsininkai);
            spausdinimas.write("Daugiausia imete tritaskiu: "
                    + tritaskininkas.getNumeris() + " "
                    + tritaskininkas.getVardas() + " "
                    + tritaskininkas.getPavarde() + " "
                    + tritaskininkas.getTritaskiai() + "\n");

            Zaidejas taikliausias = tiksliausias(krepsininkai);
            spausdinimas.write("Taikliausias: "
                    + taikliausias.getNumeris() + " "
                    + taikliausias.getVardas() + " "
                    + taikliausias.getPavarde() + " "
                    + taikliausias.getTikslumas() + "\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void skaitytiZaidejus(String zaidejuFailas,
                                        Map<Integer, Zaidejas> krepsininkai) {

        try (BufferedReader br = new BufferedReader(new FileReader(zaidejuFailas))) {
            String eilute = br.readLine();
            while (eilute != null) {
                String[] eilDuomenys = eilute.split(" ");
                Integer numeris = Integer.parseInt(eilDuomenys[0]);
                String vardas = eilDuomenys[1];
                String pavarde = eilDuomenys[2];
                Zaidejas zaidejas = new Zaidejas(numeris, vardas, pavarde);
                krepsininkai.put(numeris, zaidejas);
                eilute = br.readLine();
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println("something else went wrong");
        }
    }

    public static void skaitytiTaskus(String taskuFailas, Map<Integer,
            Zaidejas> zaidejai) {
        try (BufferedReader br = new BufferedReader(new FileReader(taskuFailas))) {
            String eilute = br.readLine();
            while (eilute != null) {
                String[] eilDuomenys = eilute.split(" ");
                Integer zaidejoNr = Integer.parseInt(eilDuomenys[0]);
                Integer taskai = Integer.parseInt(eilDuomenys[1]);
                if (zaidejai.get(zaidejoNr) != null) {
                    iterptiStatiska(zaidejai, zaidejoNr, taskai);
                }
                eilute = br.readLine();
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println("something else went wrong");
        }
    }

    public static void iterptiStatiska(Map<Integer, Zaidejas> zaidejai,
                                       Integer zaidejoNr,
                                       Integer taskai) {
        Zaidejas zaidejas = zaidejai.get(zaidejoNr);
        switch (taskai) {
            case 0:
                zaidejas.setNepataikyti(zaidejas.getNepataikyti() + 1);
                break;
            case 1:
                zaidejas.setBaudos(zaidejas.getBaudos() + 1);
                zaidejas.setTaskai(zaidejas.getTaskai() + taskai);
                break;
            case 2:
                zaidejas.setDvitaskiai(zaidejas.getDvitaskiai() + 1);
                zaidejas.setTaskai(zaidejas.getTaskai() + taskai);
                break;
            case 3:
                zaidejas.setTritaskiai(zaidejas.getTritaskiai() + 1);
                zaidejas.setTaskai(zaidejas.getTaskai() + taskai);
                break;
        }
    }

    public static Zaidejas rezultatyviausias(Map<Integer, Zaidejas> zaidejai) {
        Integer maxTaskai = 0;
        Zaidejas rezultatyviausias = null;
        for (Zaidejas zaidejas : zaidejai.values()) {
            if (maxTaskai < zaidejas.getTaskai()) {
                maxTaskai = zaidejas.getTaskai();
                rezultatyviausias = zaidejas;
            }
        }
        return rezultatyviausias;
    }

    public static Zaidejas daugiausiaTritaskiu(Map<Integer, Zaidejas> zaidejai) {
        Integer maxTritaskiai = 0;
        Zaidejas daugiausiaTritaskiu = null;
        for (Zaidejas zaidejas : zaidejai.values()) {
            if (maxTritaskiai < zaidejas.getTritaskiai()) {
                maxTritaskiai = zaidejas.getTritaskiai();
                daugiausiaTritaskiu = zaidejas;
            }
        }
        return daugiausiaTritaskiu;
    }

    public static Zaidejas tiksliausias(Map<Integer, Zaidejas> zaidejai) {
        Double maxTikslumas = 0d;
        Zaidejas tiksliausias = null;
        for (Zaidejas zaidejas : zaidejai.values()) {
            if (maxTikslumas < zaidejas.getTikslumas()) {
                maxTikslumas = zaidejas.getTikslumas();
                tiksliausias = zaidejas;
            }
        }
        return tiksliausias;
    }
}

